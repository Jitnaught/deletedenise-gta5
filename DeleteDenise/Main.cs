﻿using System;
using GTA;
using GTA.Math;

namespace DeleteDenise
{
	public class DeleteDenise : Script
	{
		readonly Model deniseModel = "ig_denise";
		readonly Vector3 denisePos = new Vector3(-11f, -1437f, 31f);

		public DeleteDenise()
		{
			Interval = 100;
			Tick += DeleteDenise_Tick;
		}
		
		void DeleteDenise_Tick(object sender, EventArgs e)
		{
			Ped plrPed = Game.Player.Character;

			if (plrPed == null || !plrPed.Exists()) return;

			Vector3 plrPos = plrPed.Position;

			if (plrPos.DistanceTo(denisePos) > 50f) return;

			Ped denise = World.GetClosestPed(plrPos, 50f, deniseModel);

			if (denise != null && denise.Exists())
			{
				denise.Delete();
			}
		}
	}
}
